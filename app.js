"use strict";

/**
 * Copyright 2017 Hewlett Packard Enterprise

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * Based on the blog here:
 * http://blog.scottlogic.com/2015/03/09/OAUTH2-Authentication-with-ADFS-3.0.html
 */

const express = require('express');
const request = require('request');
const jwt = require('jsonwebtoken');
const fs = require('fs');

/**
 * This is the signing key exported from the ADFS server
 * Validating the JWT prevents MIM attacks
 */
const adfsSigningPublicKey = fs.readFileSync('ADFS-Signing.cer'); //Get this signing key from ADFS

const app = express();

/**
 * Information to acquire from ADFS
 */
const adfsUrl = 'https://your.adfs.com';
const clientId = 'Id You Created in ADFS'; // Likely a GUID you generated
const redirectUrl = 'http://localhost:3000/auth/callback'; //This url must be in this project
const relyingPartyId = 'https://your.crm.com/'; //From ADFS Relying Party configuration, probably CRM url

const crmUrl = 'https://your.crm.com';

/**
 *  Route for the index page
 */
app.get('/', function (req, res) {
    res.write('<html>');
    res.write('<head><title>CRM-Node.js Oauth2 Demo - Login</title></head>');
    res.write('<body>');
    res.write('<h2>You are not logged in</h2>');
    res.write(`<a href="${adfsUrl}/adfs/oauth2/authorize?response_type=code&client_id=${clientId}&resource=${relyingPartyId}&redirect_uri=${redirectUrl}">Login</a>`);
    res.write('</body>');
    res.write('</html>');
    res.end();
});


/**
 * Validate the JWT using the ADFS certificate
 * @param accessToken Token acquired from login
 * @returns A user object dervied from the JWT
 */
function validateAccessToken(accessToken) {
    let payload = null;
    try {
        payload = jwt.verify(accessToken, adfsSigningPublicKey);
    }
    catch(e) {
        console.warn('Dropping unverified accessToken', e);
    }
    return payload;
}

/**
 * Get the OAUTH token from ADFS using the Code returned from login
 * @param code The code acquired from ADFS login
 * @returns {Promise}
 *
 * The clientId and redirect url must already be registered with ADFS
 */
function getTokenWithCode(code) {

    return new Promise(function(resolve, reject) {

        const adfsOptions = {
            url: `${adfsUrl}/adfs/oauth2/token`,
            followRedirect: false,
            form: {
                grant_type: 'authorization_code',
                client_id: clientId,
                redirect_uri: redirectUrl,
                code: code
            },
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        request.post(adfsOptions, function (error, response, result) {
            let adfsResponse = null;
            let payload = null;
            if (error) {
                reject(error);
            } else {
                adfsResponse = JSON.parse(result);
                payload = validateAccessToken(adfsResponse.access_token);
                if (payload === null) {
                    reject("Invalid token");
                } else {
                    resolve(adfsResponse.access_token);
                }
            }
        });
    });
}

function simpleCrmQuery(token) {

    return new Promise(function(resolve, reject) {
        const crmOptions = {
            url: `${crmUrl}/api/data/v8.1/accounts?$select=name`,
            followRedirect: false,
            headers: {
                'Accept': 'application/json',
                'OData-MaxVersion': '4.0',
                'OData-Version': '4.0',
                'Authorization': 'Bearer ' + token
            }
        };

        request.get(crmOptions, function (error, response, result) {

            if (error) {
                reject(error);
            }
            resolve(JSON.parse(result));

            return result;
        });
    });
}

/**
 * Callback handler from ADFS login
 */
app.get('/auth/callback', function (req, res) {

    res.write('<html>');
    res.write('<head><title>CRM-Node.js Oauth2 Demo - Login</title></head>');
    res.write('<body>');

    if(req.query.code !== null) {

        getTokenWithCode(req.query.code).then(

            function(token) {
                return simpleCrmQuery(token);
            }

        ).then(

            function(crmResult) {

                res.write("<table><tr><th>Name</th><th>AccountId</th></tr>");

                crmResult.value.forEach(function(item){
                   res.write(`<tr><td>${item.name}</td><td>${item.accountid}</td></tr>`);
                });
                res.write("</table>");
                res.write('</body>');
                res.write('</html>');
                res.end();
            }

        ).catch(function(err){
            res.write(err);
            res.write('</body>');
            res.write('</html>');
            res.end();
        });
    } else {
        res.write('<div>Unable to obtain access code</div>');
        res.write('</body>');
        res.write('</html>');
        res.end();
    }

});

/**
 * Start listening for requests
 */
const server = app.listen(3000, function () {
    const host = server.address().address;
    const port = server.address().port;
    console.log('App listening at http://%s:%s', host, port);
});