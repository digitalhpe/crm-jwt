**Simple OAUTH2 Dynamics CRM Example**

This sample is an overly simplistic Node.js web app that demonstrates how to login in via ADFS and then perform a simple query to CRM.

The code in this sample is based on [a blog post I found ](http://blog.scottlogic.com/2015/03/09/OAUTH2-Authentication-with-ADFS-3.0.html) while trying to solve a configuration issue.

For this to operate properly you need to export the signing certificate from ADFS by installing it to the server's certificate store and then expoorting it as a base64 encoded .cer file.

You will need to [register your app with ADFS](https://technet.microsoft.com/itpro/powershell/windows/adfs/add-adfsclient), acquiring the clientId and redirect url from that process.

Also from ADFS you need the relying party identifier from the the relying party trust for CRM.